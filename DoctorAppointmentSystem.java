package finalproject;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


class AppointmentSystem {
    private Map<String, Boolean> appointmentSlots = new HashMap<>(); // Date-Time to Availability

    public void viewAvailableSlots() {
        System.out.println("\nAvailable Slots:");

        for (Map.Entry<String, Boolean> entry : appointmentSlots.entrySet()) {
            String dateTime = entry.getKey();
            boolean isAvailable = entry.getValue();

            System.out.println(dateTime + ": " + (isAvailable ? "Available" : "Booked"));
        }
    }

    public void bookAppointment(String dateTime) {
        if (appointmentSlots.containsKey(dateTime) && appointmentSlots.get(dateTime)) {
            appointmentSlots.put(dateTime, false);
            System.out.println("Appointment booked successfully at " + dateTime + ".");
        } else {
            System.out.println("Invalid date and time or slot already booked. Please try again.");
        }
    }

    public void addAvailableSlot(String dateTime) {
        appointmentSlots.put(dateTime, true);
    }
}

public class DoctorAppointmentSystem {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
			AppointmentSystem appointmentSystem = new AppointmentSystem();

			// Sample data
			appointmentSystem.addAvailableSlot("2024-01-25 10:00");
			appointmentSystem.addAvailableSlot("2024-01-25 14:30");
			appointmentSystem.addAvailableSlot("2024-01-25 16:45");

			while (true) {
			    System.out.println("\nDoctor Appointment System");
			    System.out.println("1. View Available Slots");
			    System.out.println("2. Book an Appointment");
			    System.out.println("3. Exit");
			    System.out.print("Enter your choice: ");

			    int choice = scanner.nextInt();

			    switch (choice) {
			        case 1:
			            appointmentSystem.viewAvailableSlots();
			            break;
			        case 2:
			            System.out.print("Enter the date and time for the appointment (yyyy-MM-dd HH:mm): ");
			            scanner.nextLine(); // Consume the newline character
			            String dateTime = scanner.nextLine();
			            appointmentSystem.bookAppointment(dateTime);
			            break;
			        case 3:
			            System.out.println("Exiting program. Thank you!");
			            System.exit(0);
			        default:
			            System.out.println("Invalid choice. Please enter a valid option.");
			    }
			}
		}
    }
}
